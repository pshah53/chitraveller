# -*- coding: utf-8 -*-
# try something like
def index(): return dict(message="hello from user.py")

def profile():
    form = SQLFORM(db.profile)
    form.vars.profile_id = request.args(0)
    if form.process().accepted:
       response.flash = 'Profile Updated!'
    elif form.errors:
       response.flash = 'Duh! Try again please!'
    response.flash = T(str(auth.user_id)) # this is the user id..
    return dict(form=form)

def myprofile(): return dict(form=auth.profile())
