# -*- coding: utf-8 -*-
# try something like
def index(): return dict(message="hello from admin.py")

##################################  APPROVAL #################################

#@auth.requires_membership(role='admin')
def check_approval():
    rows = db(db.places.status=='Pending').select()
    return dict(rows=rows)

################ AJAX CALLBACK ##############        
@auth.requires_login()
def approve():
    if request.env.request_method != 'POST': raise HTTP(400)
    id = request.args(0)
    db(db.places.id==id).update(status='Approved',publish=True)
    """
    author_id = db(db.places.id==id).select(db.places.submitby).first()
    author_email = db(auth.user.id == author_id).select(auth.user.email)
    print author_email
    sent = mail.send(author_email,'Plugin Approved','Your plugin has been approved')
    """
    
def approve_comment():
    if request.env.request_method != 'POST': raise HTTP(400)
    id = request.args(0)
    db(db.comment.id==id).update(publish=True)

def list_users():
    btn = lambda row: A("Edit", _href=URL('manage_user', args=row.auth_user.id))
    db.auth_user.edit = Field.Virtual(btn)
    rows = db(db.auth_user).select()
    headers = ["ID", "Name", "Last Name", "Email", "Edit"]
    fields = ['id', 'first_name', 'last_name', "email", "edit"]
    table = TABLE(THEAD(TR(*[B(header) for header in headers])),
                  TBODY(*[TR(*[TD(row[field]) for field in fields]) \
                        for row in rows]))
    table["_class"] = "table table-striped table-bordered table-condensed"
    return dict(table=table)

def manage_user():
    user_id = request.args(0) or redirect(URL('list_users'))
    form = SQLFORM(db.auth_user, user_id).process()
    membership_panel = LOAD(request.controller,
                            'manage_membership.html',
                             args=[user_id],
                             ajax=True)
    return dict(form=form,membership_panel=membership_panel)

#@auth.requires_membership("admin") # uncomment to enable security 
def manage_membership():
    user_id = request.args(0) or redirect(URL('list_users'))
    db.auth_membership.user_id.default = int(user_id)
    db.auth_membership.user_id.writable = False
    form = SQLFORM.grid(db.auth_membership.user_id == user_id,
                       args=[user_id],
                       searchable=False,
                       deletable=False,
                       details=False,
                       selectable=False,
                       csv=False,
                       user_signature=False)
    return form


#############################################

def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request,db)
