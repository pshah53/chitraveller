# -*- coding: utf-8 -*-
# try something like
#####################################################################
# List all the places from the selected cateogry.
#####################################################################
def index():
    c = request.args(0)
    condition = (db.places.category == c)&(db.places.status == 'Approved')&(db.places.publish == True)
    rows = db(condition).select()
    plugcount = len(rows)
    msg = str(plugcount)+" Plugins found in "+c
    response.flash = msg
    return dict(rows=rows,category=c)


#####################################################################
# ../category/showplace/1 
# Show the place information selected from the list.
#####################################################################
def showplace():
    selfid = auth.user_id
    #get the content for the requested plugin
    rows = db(db.places.id == request.args(0)).select()
    title = rows[0].name
    #get the comments for the requested places
    form = SQLFORM(db.reviews)
    form.vars.place_id = request.args(0)
    if form.process().accepted:
        response.flash = 'Your review is posted'
    reviews = db(db.reviews.place_id==request.args(0)).select()
    return dict(row=rows, reviewform=form, reviews=reviews,selfid=selfid, title=title)

#############################################

def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request,db)
