# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations

#########################################################################
## This is a sample controller
## - index is the default action of any application
## - user is required for authentication and authorization
## - download is for downloading files uploaded in the db (does streaming)
## - api is an example of Hypermedia API support and access control
#########################################################################

def index():
    """
    example action using the internationalization operator T and flash
    rendered by views/default/index.html or views/generic.html

    if you need a simple wiki simply replace the two lines below with:
    return auth.wiki()
    """
    response.flash = T("Welcome to web2py!")
    #session.userid = 
    return dict(message=T('Hello World'))


def user():
    """
    exposes:
    http://..../[app]/default/user/login
    http://..../[app]/default/user/logout
    http://..../[app]/default/user/register
    http://..../[app]/default/user/profile
    http://..../[app]/default/user/retrieve_password
    http://..../[app]/default/user/change_password
    http://..../[app]/default/user/manage_users (requires membership in
    use @auth.requires_login()
        @auth.requires_membership('group name')
        @auth.requires_permission('read','table name',record_id)
    to decorate functions that need access control
    """
    return dict(form=auth())


###################################################
# SEARCH CONTROLLER
###################################################
def search():
     "an ajax wiki search page"
     return dict(form=FORM(INPUT(_id='keyword',_name='keyword',
              _onkeyup="ajax('bg_find', ['keyword'], 'target');")),
              target_div=DIV(_id='target'))

def bg_find():
     "an ajax callback that returns a <ul> of links to wiki pages"
     pattern = '%' + request.vars.keyword.lower() + '%'
     pages = db(db.places.name.lower().like(pattern))\
       .select(orderby=db.places.name)
     items = [A(row.name, _href=URL('category','show', args=row.id)) \
            for row in pages]
     return UL(*items).xml()

####################################################################

#########################################################################
## CONTACT FORM
## SET THE EMAIL PARAMETERS IN DB.PY
#########################################################################
def contact():
    form=SQLFORM(db.contactmessage,fields=['your_name','your_email','your_message'])
    if form.accepts(request.vars,session):
       subject='From contact form:'+form.vars.your_name
       email_user(sender=form.vars.your_email,\
                  message=form.vars.your_message,\
                  subject=subject)
       response.flash='your message has been submitted'
    elif form.errors:
       response.flash='please check the form and try again'
    return dict(top_message=TOP_MESSAGE,form=form)

@cache.action()
def download():
    """
    allows downloading of uploaded files
    http://..../[app]/default/download/[filename]
    """
    return response.download(request, db)


def call():
    """
    exposes services. for example:
    http://..../[app]/default/call/jsonrpc
    decorate with @services.jsonrpc the functions to expose
    supports xml, json, xmlrpc, jsonrpc, amfrpc, rss, csv
    """
    return service()


@auth.requires_login() 
def api():
    """
    this is example of API with access control
    WEB2PY provides Hypermedia API (Collection+JSON) Experimental
    """
    from gluon.contrib.hypermedia import Collection
    rules = {
        '<tablename>': {'GET':{},'POST':{},'PUT':{},'DELETE':{}},
        }
    return Collection(db).process(request,response,rules)
