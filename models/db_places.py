# -*- coding: utf-8 -*-

db.define_table('places',
                Field('name'),
                Field('descr', 'text',requires=IS_NOT_EMPTY()),
                Field('screenshot1','upload'),
                Field('screenshot2','upload'),
                Field('youtubekey'),
                Field('latitude'),
                Field('longitude'),
                Field('map_popup'),
                Field('submitby',db.auth_user,default=auth.user_id),
                Field('category',requires=IS_IN_SET(CATEGORY)),
                Field('publish',requires=IS_IN_SET(PUBLISH), default=False),
                Field('status',requires=IS_IN_SET(STATUS),default='Pending'),
                Field('average','double'),
                Field('numratings','integer'),
                Field('numcomments','integer'))

db.define_table('reviews',
                Field('place_id', 'reference places'),
                Field('title'),
                Field('review_txt'),
                Field('author',db.auth_user,default=auth.user_id),
                Field('rec_dt','datetime',default=request.now))

db.reviews.author.writable=db.reviews.author.readable=False
db.reviews.rec_dt.writable=db.reviews.rec_dt.readable=False
db.reviews.place_id.writable=db.reviews.place_id.readable=False

"""
db.define_table('reply',
    #Field('reply_id','integer'),
    Field('fthread_id','reference fthread'),
    Field('reply_body','text'),
    Field('author',db.auth_user,default=auth.user_id),
    auth.signature)
"""
