# -*- coding: utf-8 -*-
# this file is released under public domain and you can use without limitations



#########################################################################
## this is the main application menu add/remove items as required
#########################################################################

response.menu = [
    (T('Home'), False, URL('default','index'), [])
    ]

x=list()
for i in CATEGORY:
    entry=(T(i), False,URL('category', 'index/'+i))
    x.append(entry)         
    
response.menu+=[
    (T('Category'), False, URL('category', '#'),x
    # x loads subcategories dynamically
   )]

response.menu += [
    (T('Search'), False, URL('default','search'), [])
    ]

response.menu += [
    (T('Discuss'), False, URL('forum','index'), [])
    ]
response.menu += [
    (T('Contact'), False, URL('default','contact'), [])
    ]   
if auth.is_logged_in():
    response.menu += [
        (T('User Menu'), False, URL('default','user/profile'), [
        
            (T('My Plugins'), False,
                 URL('user', 'my_plugins')),
                 
            (T('Submit New Plugin'), False,
                 URL('user', 'new_plugin')),
    
        ])
        ]


#@auth.requires_membership(role='admin')
    response.menu += [
        (T('Admin'), False, URL('admin','index'), [
        
            (T('Pending Approval'), False,
                 URL('admin', 'check_approval')),
                 
            (T('Pending Comments Approval'), False,
                 URL('admin', 'check_comment_approval')),
        ])
    ]
