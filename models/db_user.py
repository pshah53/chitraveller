# -*- coding: utf-8 -*-
db.define_table('profile',
                Field('profile_id', 'reference auth_user'),
                Field('user_alias'),
                Field('profile_pic','upload'),
                Field('about_me'),
                Field('author',db.auth_user,default=auth.user_id),
                Field('rec_dt','datetime',default=request.now))

db.profile.profile_id.writable=db.profile.profile_id.readable=False
db.profile.author.writable=db.profile.author.readable=False
db.profile.rec_dt.writable=db.profile.rec_dt.readable=False
